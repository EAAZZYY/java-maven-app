def test() {
    when {
        not{
            BRANCH_NAME == 'master'
        }
    }
    echo 'starting test on $BRANCH_NAME branch'
    sh 'mvn test'
}

def buildJar() {
    when {
        expression {
            BRANCH_NAME == 'master'
        }
    }
    echo 'starting build-jar on $BRANCH_NAME branch'
    sh 'mvn package'
}

def finish() {
    echo 'finish job on $BRANCH_NAME branch'
}
return this